import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AnimalCreateComponent } from '../animal-create/animal-create.component';
import { Animal, CrudService } from '../core/services/crud.service';
import { DeleteConfirmDialogComponent } from './delete-confirm-dialog/delete-confirm-dialog.component';

@Component({
  selector: 'app-animais-list',
  templateUrl: './animais-list.component.html',
  styleUrls: ['./animais-list.component.scss']
})
export class AnimaisListComponent implements OnInit {

  data: Animal[] = [];
  displayedColumns: string[] = ['id', 'manejo', 'tag', 'opcoes'];
  filtroValue: string;

  constructor(
    private model: CrudService,
    public dialog: MatDialog,
    private toast: MatSnackBar
  ) { }

  ngOnInit() {
    this.getAnimais();
  }

  getAnimais(){
    this.model.getAnimaisList().toPromise().then(res => {
      this.data = res;
      console.log(this.data)
    })
  }

  filter() {
    console.log(this.filtroValue);
  }

  onDelete(id: number): void {
    const dialogRef = this.dialog.open(DeleteConfirmDialogComponent, {
      width: '30vw',
      data: id
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getAnimais();
        this.toast.open('Animal deletado com sucesso!', 'FECHAR', {
          duration: 1000
        });
      }
    })
  }

  createAnimal() {
    const dialogRef = this.dialog.open(AnimalCreateComponent, {
      width: '30vw',
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getAnimais();
        this.toast.open('Animal cadastrado com sucesso!', 'FECHAR', {
          duration: 2500
        });
      }
    })
  }
}
