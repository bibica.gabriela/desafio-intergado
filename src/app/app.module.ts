import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrudService } from './core/services/crud.service';
import { CrudComponent } from './shared/components/crud/crud.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnimaisListComponent } from './animais-list/animais-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import { DeleteConfirmDialogComponent } from './animais-list/delete-confirm-dialog/delete-confirm-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AnimalCreateComponent } from './animal-create/animal-create.component';


@NgModule({
  declarations: [AppComponent, CrudComponent, AnimaisListComponent, DeleteConfirmDialogComponent, AnimalCreateComponent],
  imports: [
    BrowserModule, 
    AppRoutingModule, 
    HttpClientModule, 
    ReactiveFormsModule, 
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    MatInputModule,
    FormsModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  providers: [CrudService],
  bootstrap: [AppComponent],
  exports: [ReactiveFormsModule],
  entryComponents: [DeleteConfirmDialogComponent, AnimalCreateComponent]
})

export class AppModule {}
