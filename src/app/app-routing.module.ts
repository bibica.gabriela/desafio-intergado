import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimaisListComponent } from './animais-list/animais-list.component';
import { CrudComponent } from './shared/components/crud/crud.component';


const routes: Routes = [
  {
    path: '', 
    component: AnimaisListComponent
  },
  {
    path: 'desafio',
    component: CrudComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
