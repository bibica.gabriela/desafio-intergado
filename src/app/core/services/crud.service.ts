import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

export interface Animal {
  id?: number;
  manejo: string;
  tag: string;
}

@Injectable({
  providedIn: "root"
})
export class CrudService {
  urlBase = `http://localhost:5050/api/`;

  constructor(
    private http: HttpClient
  ) { }

  getAnimaisList(): Observable<Animal[]> {    
    return this.http.get(`${this.urlBase}animal/`) as Observable<Animal[]>
  }

  deleteAnimal(id: number) {
    return this.http.delete(`${this.urlBase}animal/${id}`)
  }

  createAnimal(animal: Animal) {
    return this.http.post(`${this.urlBase}animal/`, animal)
  }
}

