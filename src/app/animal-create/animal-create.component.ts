import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CrudService } from '../core/services/crud.service';

@Component({
  selector: 'app-animal-create',
  templateUrl: './animal-create.component.html',
  styleUrls: ['./animal-create.component.scss']
})
export class AnimalCreateComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AnimalCreateComponent>,
    private model: CrudService,
    private formBuilder: FormBuilder,
    private toast: MatSnackBar
  ) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      manejo: ['', [Validators.required]],
      tag: ['', []]
    })
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.model.createAnimal(this.formGroup.value).toPromise().then(res => {
        this.dialogRef.close(true);
      }, err => {
        this.toast.open('Ocorreu um erro inesperado ao cadastar! Tente novamente.', 'FECHAR', {
          duration: 2500
        });
      })
    } else {
      this.toast.open('O formulário está inválido! Corrija e tente novamente.', 'FECHAR', {
        duration: 2500
      });
    }
  }

  close() {
    this.dialogRef.close()
  }

}
